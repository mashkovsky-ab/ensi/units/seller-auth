# Ensi Seller Auth

## Резюме

Название: Ensi Seller Auth  
Домен: Units  
Назначение: Управление авторизацией пользователей продавцов  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

Регламент работы над задачами тоже находится в [Confluence](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/477528081)

### Требования к БД

- В случае использование pgsql предварительно выполнить
```sql 
CREATE EXTENSION IF NOT EXISTS citext
```

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
|---|---|---|
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/units/job/seller-auth/  
URL: https://seller-auth-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

### Настройка авторизации

Для авторизации используется Laravel Passport, при первоначальном развороте требуется его настройка.

Необходимо:
1. Сгенерировать ключи

Генерируются командой:

`php artisan passport:keys`

С помощью енв переменной PASSPORT_KEYS_PATH можно задать путь для размещения ключей.

2. Создать клиентов

Клиенты создаются для каждого *-gui-backend. Для текущих целей при авторизации используется Password Grant Type. Соответственно для создания клиента с данным типом авторизации используется команда:

`php artisan passport:client --password`

При выполнении команды желательно задать кастомное имя клиента, но можно оставить и дефолтное. Провайдер оставляем дефолтный.

После создания клиента в консоль выводятся *Client ID* и *Client secret*. Их необходимо добавить в енв файл соответствующего *-gui-backend сервиса в виде переменных *UNITS_USER_AUTH_SERVICE_CLIENT_ID* и *UNITS_USER_AUTH_SERVICE_CLIENT_SECRET*.

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
