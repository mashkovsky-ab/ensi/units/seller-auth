<?php

namespace Database\Seeders;

use App\Domain\Users\Models\Role;
use App\Http\ApiV1\OpenApiGenerated\Enums\RoleEnum;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesInfo = [
            [
                'id' => RoleEnum::SELLER_OPERATOR,
                'title' => 'seller_operator',
            ],
            [
                'id' => RoleEnum::SELLER_ADMIN,
                'title' => 'seller_admin',
            ],
        ];

        foreach ($rolesInfo as $roleInfo) {
            $roleExist = Role::query()->where('id', $roleInfo['id'])->first();

            if (!$roleExist) {
                $role = new Role();
                $role->id = $roleInfo['id'];
            } else {
                $role = $roleExist;
            }

            $role->title = $roleInfo['title'];

            $role->save();
        }
    }
}
