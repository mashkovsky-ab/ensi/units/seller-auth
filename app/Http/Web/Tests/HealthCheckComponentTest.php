<?php

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);

test('GET /health success')
    ->get('/health')
    ->assertStatus(200)
    ->assertHeader('content-type', 'text/html; charset=UTF-8');
