<?php

namespace App\Http\ApiV1\Support\Tests\Factories;

use Ensi\TestFactories\Factory;

class FileFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'path' => $this->faker->text(),
            'name' => $this->faker->text(),
            'url' => $this->faker->url(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
