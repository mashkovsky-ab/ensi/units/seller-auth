<?php

namespace App\Http\ApiV1\Tests\Modules\Users\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;
use App\Http\ApiV1\Support\Tests\Factories\FileFactory;

class RoleFactory extends BaseApiFactory
{
    public ?FileFactory $avatarFactory = null;
    public ?array $addressFactories = null;
    public ?array $favoriteFactories = null;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->unique()->numberBetween(1, 10000000),
            'title' => $this->faker->unique()->title,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
