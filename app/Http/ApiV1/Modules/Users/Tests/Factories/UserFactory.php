<?php

namespace App\Http\ApiV1\Modules\Users\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class UserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->optionalId(),
            'seller_id' => $this->faker->randomNumber(),
            'login' => $this->faker->unique()->userName,
            'password' => '123qwe',
            'last_name' => $this->faker->optional()->lastName(),
            'active' => true,
            'first_name' => $this->faker->optional()->firstNameMale(),
            'middle_name' => $this->faker->optional()->firstNameMale(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail,
            'timezone' => $this->faker->timezone,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
