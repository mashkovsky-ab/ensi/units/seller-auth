<?php

use App\Domain\Users\Models\Role;
use App\Domain\Users\Models\User;
use App\Domain\Users\Models\UserRole;
use App\Http\ApiV1\Modules\Users\Tests\Factories\UserFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/users success', function () {
    $userData = UserFactory::new()->make();

    postJson('/api/v1/users', $userData)
        ->assertStatus(201)
        ->assertJsonPath('data.seller_id', $userData['seller_id'])
        ->assertJsonPath('data.login', $userData['login'])
        ->assertJsonPath('data.active', $userData['active'])
        ->assertJsonPath('data.last_name', $userData['last_name'])
        ->assertJsonPath('data.first_name', $userData['first_name'])
        ->assertJsonPath('data.middle_name', $userData['middle_name'])
        ->assertJsonPath('data.email', $userData['email'])
        ->assertJsonPath('data.phone', $userData['phone'])
        ->assertJsonPath('data.timezone', $userData['timezone']);

    assertDatabaseHas('users', [
        'seller_id' => $userData['seller_id'],
        'login' => $userData['login'],
        'active' => $userData['active'],
        'last_name' => $userData['last_name'],
        'first_name' => $userData['first_name'],
        'middle_name' => $userData['middle_name'],
        'email' => $userData['email'],
        'phone' => $userData['phone'],
        'timezone' => $userData['timezone'],
    ]);
});

test('GET /api/v1/users/{id}?include=roles success', function () {
    $user = User::factory()->create();
    $id = $user->id;
    $roles = Role::factory()->count(3)->create();
    foreach ($roles as $role) {
        UserRole::factory()->create([
            'user_id' => $id,
            'role_id' => $role->id,
        ]);
    }

    getJson("/api/v1/users/$id?include=roles", )
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonCount($roles->count(), 'data.roles');
});

test('GET /api/v1/users/{id} 404', function () {
    getJson("/api/v1/users/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('PATCH /api/v1/users/{id} success', function () {
    $login = 'test1234';
    $user = User::factory()->create(['seller_id' => 1, 'login' => $login]);
    $id = $user->id;

    $userData = UserFactory::new()
        ->only(['seller_id'])
        ->make(['seller_id' => 2]);

    patchJson("/api/v1/users/$id", $userData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.seller_id', $userData['seller_id'])
        ->assertJsonPath('data.login', $login);

    assertDatabaseHas('users', [
        'id' => $id,
        'seller_id' => $userData['seller_id'],
        'login' => $login,
    ]);
});

test('PATCH /api/v1/users/{id} 404', function () {
    patchJson("/api/v1/users/13", UserFactory::new()->make())
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('DELETE /api/v1/users/{id} success', function () {
    $user = User::factory()->create();
    $id = $user->id;

    deleteJson("/api/v1/users/$id")->assertStatus(200);

    assertModelMissing($user);
});

test('DELETE /api/v1/users/{id} 404', function () {
    deleteJson("/api/v1/users/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/users:search success", function () {
    $users = User::factory()
        ->count(10)
        ->sequence(
            ['active' => true],
            ['active' => false],
        )
        ->create();
    $lastId = $users->last()->id;

    $requestBody = [
        "filter" => [
            "active" => false,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/users:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $lastId)
        ->assertJsonPath('data.0.active', false);
});

test("POST /api/v1/users:search-one success", function () {
    $user = User::factory()->create(['active' => true]);

    $requestBody = [
        "filter" => [
            "active" => true,
        ],
    ];

    postJson("/api/v1/users:search-one", $requestBody)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $user->id)
        ->assertJsonPath('data.active', true);
});

test("POST /api/v1/users/{id}:add-roles success", function () {
    $user = User::factory()->create();
    $id = $user->id;

    $role = Role::factory()->create();

    $requestBody = [
            "roles" => [$role->id],
            "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/$id:add-roles", $requestBody)
        ->assertStatus(200);

    assertDatabaseHas('role_user', [
        'user_id' => $id,
        'role_id' => $role->id,
    ]);
});

test('POST /api/v1/users/{id}:add-roles 400', function () {
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        "roles" => [13],
        "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/$id:add-roles", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/users/{id}:add-roles 404', function () {
    $role = Role::factory()->create();

    $requestBody = [
        "roles" => [$role->id],
        "expires" => now()->addDay(),
    ];

    postJson("/api/v1/users/13:add-roles", $requestBody)
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/users/{id}:delete-role success", function () {
    $user = User::factory()->create();
    $id = $user->id;

    $role = Role::factory()->create();

    UserRole::factory()->create([
        'user_id' => $id,
        'role_id' => $role->id,
    ]);

    $requestBody = [
        "role_id" => $role->id,
    ];

    postJson("/api/v1/users/$id:delete-role", $requestBody)
        ->assertStatus(200);

    assertDatabaseMissing('role_user', [
        'user_id' => $id,
        'role_id' => $role->id,
    ]);
});

test('POST /api/v1/users/{id}:delete-role 400', function () {
    $user = User::factory()->create();
    $id = $user->id;

    $requestBody = [
        "role_id" => 13,
    ];

    postJson("/api/v1/users/$id:delete-role", $requestBody)
        ->assertStatus(400)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "ValidationError");
});

test('POST /api/v1/users/{id}:delete-role 404', function () {
    $role = Role::factory()->create();

    $requestBody = [
        "role_id" => $role->id,
    ];

    postJson("/api/v1/users/13:delete-role", $requestBody)
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test('GET /api/v1/roles/{id} success', function () {
    $role = Role::factory()->create();
    $id = $role->id;

    getJson("/api/v1/roles/$id", )
        ->assertStatus(200)
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/roles/{id} 404', function () {
    getJson("/api/v1/roles/13")
        ->assertStatus(404)
        ->assertJsonPath('data', null)
        ->assertJsonPath('errors.0.code', "NotFoundHttpException");
});

test("POST /api/v1/roles:search success", function () {
    $roles = Role::factory()
        ->count(5)
        ->create();
    $id = $roles->first()->id;

    $requestBody = [
        "filter" => [
            "id" => $id,
        ],
        "sort" => [
            "-id",
        ],
    ];

    postJson("/api/v1/roles:search", $requestBody)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $id);
});
