<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id = (int) $this->route('userId');

        return [
            'seller_id' => ['required', 'integer'],
            'login' => [Rule::unique('users')->ignore($id)],
            'active' => ['boolean'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'phone' => ['regex:/^\+7\d{10}$/', Rule::unique('users')->ignore($id)],
            'email' => ['email', Rule::unique('users')->ignore($id)],
            'timezone' => ['sometimes', 'required', 'timezone'],
        ];
    }
}
