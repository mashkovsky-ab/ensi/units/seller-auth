<?php

namespace App\Http\ApiV1\Modules\Users\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateUserRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['required', 'integer'],
            'login' => ['required', 'unique:users'],
            'active' => ['boolean'],
            'password' => ['required'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'timezone' => ['required', 'timezone'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/', 'unique:users'],
            'email' => ['required', 'email', 'unique:users'],
        ];
    }
}
