<?php

namespace App\Http\ApiV1\Modules\Users\Controllers;

use App\Http\ApiV1\Modules\Users\Queries\RolesQuery;
use App\Http\ApiV1\Modules\Users\Resources\RolesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class RolesController
{
    public function get(int $roleId, RolesQuery $query)
    {
        return new RolesResource($query->findOrFail($roleId));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, RolesQuery $query)
    {
        return RolesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
