<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'seller_id' => $this->faker->randomNumber(),
            'login' => $this->faker->unique()->userName,
            'password' => '123qwe',
            'last_name' => $this->faker->optional()->lastName(),
            'active' => true,
            'first_name' => $this->faker->optional()->firstNameMale(),
            'middle_name' => $this->faker->optional()->firstNameMale(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail,
            'timezone' => $this->faker->timezone,
        ];
    }
}
