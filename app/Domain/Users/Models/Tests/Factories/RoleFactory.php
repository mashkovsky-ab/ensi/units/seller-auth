<?php

namespace App\Domain\Users\Models\Tests\Factories;

use App\Domain\Users\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->unique()->randomNumber(1,1000),
            'title' => $this->faker->unique()->title,
        ];
    }
}
