<?php

namespace App\Domain\Users\Models;

use App\Domain\Users\Models\Tests\Factories\UserRoleFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property Carbon|null $expires
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read User $user
 */
class UserRole extends Pivot
{
    protected $dates = ['expires'];

    protected $table = 'role_user';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    public static function factory(): UserRoleFactory
    {
        return UserRoleFactory::new();
    }
}
